# Nginx Proxy Manager Example

The project demonstrates how to use the NPM to reverse proxy to docker containers that are attached to the `proxynet` network

## Initialize Docker

Run `docker compose up -d` in the npm directory to start Nginx Proxy Manager

Access the Nginx Proxy Manager via public IP on port 81 (for example http://1.2.3.4:80/)

The default credentials are: 

Email:    admin@example.com
Password: changeme

You will be prompted to change the default credentials after the first login.

Now you can run `docker compose up -d` in the project_1 and project_2 directories.

## Setup a proxy host

Create a subdomain, for example myapp.mydomain.com, point to the public IP where NPM is running.

Set up a Host record with your desired domain

![proxy host](_assets/proxy_host.png)

*Note that the "myapp" corresponds to the name of your service from docker-compose.yaml*

Now set up the SSL tab to get the SSL certificate for the domain:

![proxy ssl](_assets/proxy_ssl.png)

Your subdomain should be up now - NPM is reverse-proxying the traffic towards the Docker container and securing it with SSL.

## Dedicated database for NPM

There is a [docker-compose.mariadb.yaml](./npm/docker-compose.mariadb.yaml) if you want to use a dedicated database for everything NPM related. Be sure to change the credentials appropriately.